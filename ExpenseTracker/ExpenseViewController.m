//
//  ExpenseViewController.m
//  ExpenseTracker
//
//  Created by Tyler Barnes on 2/17/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import "ExpenseViewController.h"

@interface ExpenseViewController ()

@end

@implementation ExpenseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _expenseTable.translatesAutoresizingMaskIntoConstraints = NO;
    
    _expenseTable.dataSource = self;
    _expenseTable.delegate = self;
    
    _expenseTable.allowsMultipleSelectionDuringEditing = NO;

    
    self.navigationItem.rightBarButtonItem = self.editButtonItem;

    
    [self.view addSubview:_expenseTable];
    
    NSDictionary *viewVisualFormDict = NSDictionaryOfVariableBindings(_expenseTable);
    
    NSDictionary *metricsDict = @{};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_expenseTable]|" options:0 metrics:metricsDict views:viewVisualFormDict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_expenseTable]|" options:0 metrics:metricsDict views:viewVisualFormDict]];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];

    [self loadData];
}

-(void)loadData {
    
    AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    NSFetchRequest * request = [[NSFetchRequest alloc] initWithEntityName:@"Expense"];
    NSArray* array = [[context executeFetchRequest:request error:nil] mutableCopy];
    
    _expenseArray = [array mutableCopy];
    
    [_expenseTable reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _expenseArray.count;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }

    NSManagedObject* object = _expenseArray[indexPath.row];

    cell.textLabel.text = [object valueForKey:@"location"];

    return cell;
    
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewCellEditingStyleDelete;
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier  isEqual:@"oldExp"]){
        NSIndexPath *indexPath = [_expenseTable indexPathForCell:sender];
        ExpenseDetailViewController * edvc = segue.destinationViewController;
        edvc.expense = _expenseArray[indexPath.row];
    }
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated{
    [super setEditing:editing animated:animated];
    
    _toolbarButtons = [self.toolbarItems mutableCopy];
    
    [_expenseTable setEditing:editing animated:YES];
    
    if (editing) {
        _addButton.enabled = NO;
    } else {
        _addButton.enabled = YES;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // If row is deleted, remove it from the list.
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObject * deletingObject = _expenseArray[indexPath.row];
        NSManagedObjectContext *context = [deletingObject managedObjectContext];
        [context deleteObject:deletingObject];
        
        NSError *error;
        if (![context save:&error]) {
            NSLog(@"Object Failed to Delete!");
        }
        
//        [self loadData];
        [UIView transitionWithView: _expenseTable duration: 0.35f options: UIViewAnimationOptionTransitionCrossDissolve animations: ^(void){
            [self loadData];
            if (_expenseArray.count == 0) {
                [tableView setEditing:NO animated:YES];
            }
        } completion:nil ];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

@end
