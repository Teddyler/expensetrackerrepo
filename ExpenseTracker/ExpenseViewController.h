//
//  ExpenseViewController.h
//  ExpenseTracker
//
//  Created by Tyler Barnes on 2/17/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ExpenseDetailViewController.h"

@interface ExpenseViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *expenseTable;

@property (strong, nonatomic) NSMutableArray* expenseArray;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *addButton;
@property (strong, nonatomic) NSMutableArray *toolbarButtons;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *editButton;

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;

@end
