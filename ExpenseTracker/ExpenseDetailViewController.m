//
//  ExpenseDetailViewController.m
//  ExpenseTracker
//
//  Created by Tyler Barnes on 2/16/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import "ExpenseDetailViewController.h"
#import "AppDelegate.h"

@interface ExpenseDetailViewController ()

@end

@implementation ExpenseDetailViewController

@synthesize revealDatePicker;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _datePicker.hidden = true;
    _location.delegate = self;
    _location.returnKeyType = UIReturnKeyDone;
    _cost.delegate = self;
    _cost.returnKeyType = UIReturnKeyDone;
    _notes.delegate = self;
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    _selectedDate.text = [NSString stringWithFormat:@"%@",[formatter stringFromDate:_datePicker.date]];
    
    if(self.expense){
        _location.text = [self.expense valueForKey:@"location"];
        _cost.text = [self.expense valueForKey:@"cost"];
        _datePicker.date = [self.expense valueForKey:@"date"];
        _notes.text = [self.expense valueForKey:@"notes"];
        _selectedDate.text = [NSString stringWithFormat:@"%@",[formatter stringFromDate:[self.expense valueForKey:@"date"]]];
    }
    
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(TextViewDoneButtonPressed)];
    keyboardToolbar.items = @[flexBarButton, doneBarButton];
    self.notes.inputAccessoryView = keyboardToolbar;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveExpense:(UIBarButtonItem *)sender {
    
    AppDelegate* appD = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = appD.persistentContainer.viewContext;
    
    if (self.expense) {
        [self.expense setValue:_location.text forKey:@"location"];
        [self.expense setValue: _cost.text forKey:@"cost"];
        [self.expense setValue:_datePicker.date forKey:@"date"];
        [self.expense setValue:_notes.text forKey:@"notes"];
    } else {
        //Create a new managed object
        NSManagedObject * newExpense = [NSEntityDescription insertNewObjectForEntityForName:@"Expense" inManagedObjectContext:context];
        [newExpense setValue:_location.text forKey:@"location"];
        [newExpense setValue: _cost.text forKey:@"cost"];
        [newExpense setValue:_datePicker.date forKey:@"date"];
        [newExpense setValue:_notes.text forKey:@"notes"];
        
    }

    
    NSError *error = nil;
    if (![context save:&error]) {
        NSAssert(NO, @"Error saving context: %@", [error localizedDescription]);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)cancelButton:(UIBarButtonItem *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)showDatePicker:(UIButton *)sender {
    if(_datePicker.hidden == true){
        _datePicker.hidden = false;
        _datePicker.backgroundColor = [UIColor lightGrayColor];
        [_notes resignFirstResponder];
    }
    else if(_datePicker.hidden == false){
        _datePicker.hidden = true;
    }
}

- (IBAction)datePicker:(id)sender {
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    _selectedDate.text = [NSString stringWithFormat:@"%@",[formatter stringFromDate:_datePicker.date]];
    
}
- (IBAction)emailExpense:(UIBarButtonItem *)sender {
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    NSString * date = [NSString stringWithFormat:@"%@", [formatter stringFromDate:_datePicker.date]];
    
    MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
    mailCont.mailComposeDelegate = self;
    
    [mailCont setSubject:[NSString stringWithFormat:@"Expense at %@", _location.text]];
    NSString* message = [NSString stringWithFormat:@", \n\nLocation: %@ \nCost: %@ \nDate: %@ \nNotes: %@ \n\n", _location.text, _cost.text, date, _notes.text];
    
    [mailCont setMessageBody:message isHTML:NO];
    
    [self presentViewController:mailCont animated:YES completion:^{
        
    }];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if(![touch.view isMemberOfClass:[UITextField class]]) {
        [touch.view endEditing:YES];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)TextViewDoneButtonPressed
{
    [_notes resignFirstResponder];
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    
    if(_datePicker.hidden == false){
        _datePicker.hidden = true;
    }
}

@end
