//
//  ExpenseDetailViewController.h
//  ExpenseTracker
//
//  Created by Tyler Barnes on 2/16/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <Messages/Messages.h>
#import <MessageUI/MessageUI.h>

@interface ExpenseDetailViewController : UIViewController <MFMailComposeViewControllerDelegate, UITextFieldDelegate, UITextViewDelegate>

//Core Data Dictionary
@property NSDictionary* ExpenseDictionary;

//properties to save to Core Data
@property (strong, nonatomic) IBOutlet UITextField *location;
@property (strong, nonatomic) IBOutlet UITextField *cost;
@property (strong, nonatomic) IBOutlet UILabel *selectedDate;
@property (strong, nonatomic) IBOutlet UITextView *notes;
@property (strong, nonatomic) IBOutlet UIButton *revealDatePicker;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (strong,nonatomic) NSManagedObjectContext* context;
@property (strong, nonatomic) NSArray* expenseArray;
@property (strong, nonatomic) NSManagedObject* expense;

//Changes selectedDateLabel
- (IBAction)datePicker:(UIDatePicker *)sender;

//Reveals Date Picker
- (IBAction)showDatePicker:(UIButton *)sender;

//Saves values to CoreData and dismisses the DetailViewController
- (IBAction)saveExpense:(UIBarButtonItem *)sender;

//Cancels Added Expense and dismisses all ViewControllers
- (IBAction)cancelButton:(UIBarButtonItem *)sender;

//Launch Email
- (IBAction)emailExpense:(UIBarButtonItem *)sender;

@end
